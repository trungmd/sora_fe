import axios from 'axios';

export default {
    data() {
        return {
            dataEmployeeList: null,
            detailEmployee: null,
        };
    },
    methods: {
        getDataEmployeeList(pageSize, pageNumber, employeeFilter) {
            axios
                .get('https://cukcuk.manhnv.net/api/v1/Employees/filter', {
                    params: {
                        pageSize: pageSize,
                        pageNumber: pageNumber,
                        employeeFilter: employeeFilter,
                    },
                })
                .then((response) => {
                    this.responseData = response.data;
                });
        },
        getDetailEmployee(id) {
            axios
                .get(`https://cukcuk.manhnv.net/api/v1/Employees/${id}`)
                .then((response) => {
                    this.detailEmployee = response.data;
                });
        },
    },
    mounted() {
        this.getDataEmployeeList();
        this.getDetailEmployee();
    },
};

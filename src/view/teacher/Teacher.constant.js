const Degree = {
    university: 'Đại học',
    master: 'Thạc sĩ',
    doctor: 'Tiến sĩ',
};

const Gender = {
    male: 'Nam',
    female: 'Nữ',
};

export { Degree, Gender };

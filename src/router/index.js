import AuthLayout from '@/layout/AuthLayout.vue';
import MainLayout from '@/layout/MainLayout.vue';
import LoginViewAuthen from '@/view/authen/LoginView.vue';
import CourseListVue from '@/view/course/CourseList.vue';
import DashboardListVue from '@/view/dashboard/DashboardList.vue';
import EmployeeListVue from '@/view/employee/EmployeeList.vue';
import ManagerListVue from '@/view/manager/ManagerList.vue';
import StudentDetail from '@/view/student/StudentDetail.vue';
import StudentListVue from '@/view/student/StudentList.vue';
import SyllabusListVue from '@/view/syllabus/SyllabusList.vue';
import TeacherDetail from '@/view/teacher/TeacherDetail.vue';
import TeacherListVue from '@/view/teacher/TeacherList.vue';
import ForgotPassword from '@/view/authen/ForgotPassword.vue';

// import { isAuthenticated } from './authMiddleware';
export const routes = [
    {
        path: '/',
        component: AuthLayout,
        children: [
            {
                path: 'login',
                component: LoginViewAuthen,
                name: 'login',
            },
            {
                path: 'forgot-password',
                component: ForgotPassword,
                name: 'forgot-password',
            },
        ],
    },
    {
        path: '/',
        component: MainLayout,
        meta: { requiresAuth: true },
        children: [
            {
                path: 'dashboard',
                name: 'dashboard',
                component: DashboardListVue,
            },
            { path: '/employee', component: EmployeeListVue },
            {
                path: '/student',
                component: StudentListVue,
            },
            {
                path: '/student/:id',
                component: StudentDetail,
                name: 'student-detail',
            },
            { path: '/teacher', component: TeacherListVue },
            {
                path: '/teacher/:id',
                component: TeacherDetail,
                name: 'teacher-detail',
            },
            { path: '/manager', component: ManagerListVue },
            { path: '/course', component: CourseListVue },
            {
                path: '/syllabus',
                component: SyllabusListVue,
            },
        ],
    },
    // { path: '/', component: DashboardListVue },
];
